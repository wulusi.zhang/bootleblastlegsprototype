﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameObject ScoreCounter;
    [SerializeField]
    private float ScoreAmt, DeductAmt;
    [SerializeField]
    private GameObject LaneContainer;

    [SerializeField]
    private List<GameObject> Lanes = new List<GameObject>();
    [SerializeField]
    private GameObject SpawnerConainer;

    [SerializeField]
    private List<GameObject> Spawners = new List<GameObject>();
    [SerializeField]
    private List<GameObject> Tokens = new List<GameObject>();
    [SerializeField]
    private List<GameObject> ActiveTokens = new List<GameObject>();

    [SerializeField]
    [Range(0,15)]
    private float TokenSpawnDelayTimer;
    [SerializeField]
    [Range(0,10)]
    private float TokenSpeed;

    [SerializeField]
    GameObject GameCompleteScreen;

    bool canSpawnTokens = true;

    #region Singleton Logic
    public static GameManager instance = null;
    private void SingletonCheck()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }
    #endregion

    // Start is called before the first frame update
    void Awake()
    {
        SingletonCheck();
    }
    private void Start()
    {
        StartCoroutine(SpawnTokenSequence());
    }
    public void RaiseScore()
    {
        if (ScoreCounter != null)
        {
            ScoreCounter.GetComponent<Slider>().value += ScoreAmt;
        }
        CheckForWin();
    }

    public void CheckForWin()
    {
        if(ScoreCounter.GetComponent<Slider>().value >= 1 && ScoreCounter != null)
        {
            GameCompleteScreen.SetActive(true);
            canSpawnTokens = false;
        }
    }

    public void RaiseScoreNeutral()
    {
        if (ScoreCounter != null)
        {
            ScoreCounter.GetComponent<Slider>().value += (ScoreAmt / 2);
        }
        CheckForWin();
    }
    
    //Used by button
    public void ReactivateScene()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        for (int i = 0; i < ActiveTokens.Count; i++)
        {
            Destroy(ActiveTokens[i].gameObject);
        }
        GameCompleteScreen.SetActive(false);
        StopAllCoroutines();
        ScoreCounter.GetComponent<Slider>().value = 0;
        ActiveTokens.Clear();
        canSpawnTokens = true;
        StartCoroutine(SpawnTokenSequence());
    }

    public void DeductScore()
    {
        ScoreCounter.GetComponent<Slider>().value -= (DeductAmt);
    }

    private IEnumerator SpawnTokenSequence()
    {
        yield return new WaitForSeconds(TokenSpawnDelayTimer*2);
        while (canSpawnTokens)
        {
            GameObject SpanwedToken = Instantiate(Tokens[Random.Range(0, Tokens.Count)], Spawners[Random.Range(0, Spawners.Count)].transform);
            SpanwedToken.GetComponent<TokenBehaviour>().SetSpeed(TokenSpeed);
            if(!ActiveTokens.Contains(SpanwedToken))
            ActiveTokens.Add(SpanwedToken);
            yield return new WaitForSeconds(TokenSpawnDelayTimer);
        }
    }

    public void RemoveActiveToken(GameObject tokenToRemove)
    {
        if(ActiveTokens.Contains(tokenToRemove))
        {
            ActiveTokens.Remove(tokenToRemove);
        }
    }

    private void OnValidate()
    {
        if (LaneContainer != null)
        {
            for (int i = 0; i < LaneContainer.transform.childCount; i++)
            {
                GameObject child = LaneContainer.transform.GetChild(i).gameObject;
                if (!Lanes.Contains(child))
                {
                    Lanes.Add(child);
                }
            }
        }

        if (SpawnerConainer != null)
        {
            for (int i = 0; i < SpawnerConainer.transform.childCount; i++)
            {
                GameObject child = SpawnerConainer.transform.GetChild(i).gameObject;
                if (!Spawners.Contains(child))
                {
                    Spawners.Add(child);
                }
            }
        }

    }
    public List<GameObject> GetLanes()
    {
        if (Lanes.Count != 0)
        {
            return Lanes;
        }
        else 
        {
            return null;
        }
    }
}
