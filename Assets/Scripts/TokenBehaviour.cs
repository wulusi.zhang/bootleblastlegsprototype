﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TokenBehaviour : MonoBehaviour
{
    [SerializeField]
    private int tokenID;

    private float speed;
    public float GetTokenID()
    {
        return tokenID;
    }

    public void SetSpeed(float setSpd)
    {
        speed = setSpd;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position += Vector3.down * Time.deltaTime * speed;
    }
}
