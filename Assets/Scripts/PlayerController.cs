﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private int PosIndex, StartIndex;

    [SerializeField]
    Transform currentPos, nextPos;

    [SerializeField]
    float timer, lerpSpeed, distanceCheck;
    [SerializeField]
    bool canAssign = true;
    [SerializeField]
    KeyCode Right;
    [SerializeField]
    KeyCode Left;

    [SerializeField]
    private int PlayerID;
    void Start()
    {
        SetStartPost();
    }

    void SetStartPost()
    {
        currentPos = GameManager.instance.GetLanes()[StartIndex].transform;

        this.transform.position = currentPos.transform.position;
    }
    // Update is called once per frame
    void Update()
    {
        DetectControls();
        if (!canAssign)
        {
            ResetParams();
            ChangeLane();
        }
    }

    void DetectControls()
    {
        if (Input.GetKeyDown(Left))
        {
            AssignLanes(false);
        }

        if (Input.GetKeyDown(Right))
        {
            AssignLanes(true);
        }
    }

    void ResetParams()
    {
        if (nextPos != null)
        {
            distanceCheck = Vector3.Distance(this.transform.position, nextPos.position);
            if (distanceCheck == 0)
            {
                canAssign = true;
                currentPos = nextPos;
                timer = 0;
            }
        }
    }

    void AssignLanes(bool isRight)
    {
        if (canAssign)
        {
            if (isRight)
            {
                PosIndex++;
                int maxLaneCount = (GameManager.instance.GetLanes().Count - 1);
                if (PosIndex > maxLaneCount)
                {
                    PosIndex = maxLaneCount;
                }
                nextPos = GameManager.instance.GetLanes()[PosIndex].transform;
                canAssign = false;
            }
            else
            {
                PosIndex--;
                if (PosIndex < 0)
                {
                    PosIndex = 0;
                }
                nextPos = GameManager.instance.GetLanes()[PosIndex].transform;
                canAssign = false;
            }
        }
    }

    void ChangeLane()
    {
        if (currentPos != nextPos && nextPos != null)
        {
            timer += Time.deltaTime * lerpSpeed;
            this.transform.position = Vector3.Lerp(currentPos.position, nextPos.position, timer);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        TokenBehaviour token = collision.GetComponent<TokenBehaviour>();

        if (token != null)
        {
            if (token.GetTokenID() != PlayerID)
            {
                switch (token.GetTokenID())
                {
                    case 0:
                        Destroy(collision.gameObject);
                        GameManager.instance.RaiseScoreNeutral();
                        break;
                    case 1:
                        Destroy(collision.gameObject);
                        GameManager.instance.RaiseScoreNeutral();
                        break;
                    case 2:
                        Destroy(collision.gameObject);
                        GameManager.instance.DeductScore();
                        break;
                }
            }
            else
            {
                Destroy(collision.gameObject);
                GameManager.instance.RaiseScore();
            }
        }
    }
}
